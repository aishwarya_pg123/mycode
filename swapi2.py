#!/usr/bin/env python3
"""Alta3 Research
   Star Wars API HTTP response parsing"""

# pprint makes dictionaries a lot more human readable
from pprint import pprint

# requests is used to send HTTP requests (get it?)
import requests

#URL = "https://swapi.dev/luke/force"      # Comment out this line
URL= "https://swapi.dev/api/people/4/"     # Uncomment this line

def main():
    """sending GET request, checking response"""

    # SWAPI response is stored in "resp" object
    resp= requests.get(URL)

    # check to see if the status is anything other than what we want, a 200 OK
    if resp.status_code == 200:
        # convert the JSON content of the response into a python dictionary
        vader= resp.json()
        pprint(vader)
        print("{} was born in the year {}. His eyes are now {} and his hair color is {}.".format(vader["name"], vader["birth_year"],vader["eye_color"],vader["hair_color"]))
  
        print(requests.get(vader["films"][1])
    
    else:
        print("That is not a valid URL.")

if __name__ == "__main__":
    main()

