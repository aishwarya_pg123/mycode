#!/usr/bin/python3
"""Alta3 Research | <your name here>
   Using an HTTP GET to determine when the ISS will pass over head"""

# python3 -m pip install requests
import requests

def main():
    """your code goes below here"""
    
    # stuck? you can always write comments
    # Try describe the steps you would take manually
    URL =" http://api.open-notify.org/iss-pass.json"
    lati = float(input("Enter the latitude"))
    lon = float(input("Enter the longitude"))
    res = requests.get(f"{URL}?lat={lati}&lon={lon}")
    data = res.json()
    print(data)
    my_time = data["request"]["datetime"]
    rise_time = data["response"]
    for i in rise_time:
       if my_time== i["risetime"]:
           print("ISS overhead")
       else:
           print("ISS not overhead")
    

if __name__ == "__main__":
    main()
    


