"""Your script alta3research-flask01.py should demonstrate proficiency with the flask library.
Ensure your application has at least two endpoints. At least one of your endpoints should return legal JSON."""
import json

from flask import Flask, jsonify
from flask import make_response
from flask import request
from flask import render_template
from flask import redirect
from flask import url_for

app = Flask(__name__)

favorite_pokemons= [{
    "name": "Squirtle",
    "number": 7,
    "pokemon_type": "Water",
    "weaknesses": ["Grass", "Electric"],
    "evolutions": [
        "Wartortle",
        "Blastoise"
              ]
             }]
# entry point for our users

@app.route("/")
def index():
    return render_template("login.html")


@app.route("/poke/<string:name>")
def cool_pokemon(name):
    return "Look at me, I am demonstrating proficiency with the flask library!"


@app.route("/pokemon", methods=["GET", "POST"])
def get_pokemon():
    if request.method == "POST":
        data = request.json
        if data:
            data = json.loads(data)
            name = data["name"]
            number = data["number"]
            pokemon_type = data["pokemon_type"]
            weaknesses = data["weaknesses"]
            evolutions = data["evolutions"]
            favorite_pokemons.append(
                {"name": name, "number": number, "pokemon_type": pokemon_type, "weaknesses": weaknesses,
                 "evolutions": evolutions})
    return jsonify(favorite_pokemons)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=2000)
